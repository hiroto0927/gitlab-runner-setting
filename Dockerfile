FROM gitlab/gitlab-runner:latest

RUN apt-get update && apt-get install -y docker.io

ARG CI_SERVER_URL
ARG REGISTRATION_TOKEN
ENV CI_SERVER_URL=$CI_SERVER_URL
ENV REGISTRATION_TOKEN=$REGISTRATION_TOKEN


RUN gitlab-runner register \
  --non-interactive \
  --url ${CI_SERVER_URL} \
  --token  ${REGISTRATION_TOKEN} \
  --executor "docker" \
  --docker-image alpine:latest \
  --description "docker-runner"



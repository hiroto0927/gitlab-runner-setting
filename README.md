# GitLab-Runner作成用プロジェクト

本プロジェクトは、GitLab-Runnerを作成するためのプロジェクトです。

1. 本プロジェクトをクローンします。
2. `.env`ファイルを用意します。
3. `.env`ファイル内に下記のプロパティを設定します。

| 変数名 | 説明 |
|-------|------|
|COMPOSE_PROJECT_NAME| docker-composeのプロジェクト名|
|CI_SERVER_URL| GitLabのURL|
|REGISTRATION_TOKEN|GitLabから発行されたトークン|

4. 下記のコマンドを実行します。
```
docker compose up
```

これでGitlab Runnerの登録が完了です。